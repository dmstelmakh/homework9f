package ru.inordic.arj.mvn.homework9;

public interface Linked<E> {
    void addLast(E e) throws Exception;
    int size();
    E getElementByIndex(int counter) throws Exception;
    void setElementAtPosition(int сounter, E e) throws Exception;
    void deleteElement (int counter) throws Exception;
}
