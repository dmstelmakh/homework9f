package ru.inordic.arj.mvn.homework9;

public class ListHub<E> implements Linked<E> {
    private Hub<E> firstHub;
    private Hub<E> lastHub;
    private int size = 0;

    public ListHub() {
        lastHub = new Hub<E>(null, firstHub, null);
        firstHub = new Hub<E>(null, null, lastHub);

    }

    @Override
    public void addLast(E e) throws Exception {
        Hub<E> previousElement = lastHub;
        previousElement.setCurrentValue(e);
        lastHub = new Hub<E>(null, previousElement, null);
        previousElement.setNextElement(lastHub);
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E getElementByIndex(int counter) {
        Hub<E> pocket = firstHub.getNextElement();

        try {

            for (int i = 0; i < counter; i++) {
                pocket = pickUpElement(pocket);
            }
        } catch (NullPointerException e) {
            System.out.println("вы ввели не верную позицию. Максимальная позиция " + (size() - 1));

        }
        return pocket.getCurrentValue();

    }

    private Hub<E> pickUpElement(Hub<E> current) {
        return current.getNextElement();
    }

    @Override
    public void setElementAtPosition(int counter, E e)  {
        Hub<E> newHub = new Hub<E>(e, firstHub, lastHub);
         try {
            Hub<E> pocket = firstHub.getNextElement();
            for (int i = 0; i < counter; i++) {
                pocket = pickUpElement(pocket);
            }
            pocket.setCurrentValue(e);
            pocket.getPreviousElement().setNextElement(pocket);
            pocket.getNextElement().setPreviousElement(pocket);
            size = size + 1;
        } catch (NullPointerException e1) {
            System.out.println("вы ввели не верную позицию. Максимальная позиция " + (size() - 1));
        }
    }

    @Override
    public void deleteElement(int counter) {
        try {
            Hub<E> pocket = firstHub.getNextElement();
            for (int i = 0; i < counter; i++) {
                pocket = pickUpElement(pocket);
            }
            pocket.getPreviousElement().setNextElement(pocket.getNextElement());
            pocket.getNextElement().setNextElement(pocket);
            size = size - 1;
        } catch (Exception e) {
            System.out.println("Введен неверный номер позиции. Максимальная позиция " + (size() - 1));
        }


    }

    private class Hub<E> {
        private E currentValue;
        private Hub<E> nextElement;
        private Hub<E> previousElement;

        private Hub(E currentElement, Hub<E> previousElement, Hub<E> nextElement) {
            this.currentValue = currentElement;
            this.previousElement = previousElement;
            this.nextElement = nextElement;
        }

        public E getCurrentValue() {
            return currentValue;
        }

        public void setCurrentValue(E currentValue) {
            this.currentValue = currentValue;
        }

        public Hub<E> getNextElement() {
            return nextElement;
        }

        public void setNextElement(Hub<E> nextElement) {
            this.nextElement = nextElement;
        }

        public Hub<E> getPreviousElement() {
            return previousElement;
        }

        public void setPreviousElement(Hub<E> previousElement) {
            this.previousElement = previousElement;
        }
    }
}
