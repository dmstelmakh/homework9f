package ru.inordic.arj.mvn.homework9;


import java.util.InputMismatchException;
import java.util.Scanner;

public class ListMain {
    public static void main(String[] args) throws Exception {
        System.out.println("1 - добавить элемент (в конец)");
        System.out.println("2 - добавить элемент (в позицию)");
        System.out.println("3 - удалить элемент");
        System.out.println("4 - получить размер списка");
        System.out.println("5 - получить значение элемента по позиции");
        System.out.println("6 - выйти из класса");
        Scanner scan = new Scanner(System.in);
        ListHub spisok = new ListHub();
        int operNumb = 0;
        do {
            try {
                System.out.println("Пожалуйста, введите номер операции.");
                operNumb = scan.nextInt();
                switch (operNumb) {
                    case 1:
                        System.out.println("Пожалуйста, введите элемент");
                        int number = scan.nextInt();
                        spisok.addLast(number);


                        break;
                    case 2:
                        System.out.println("   Введите номер позиции (0 - " + (spisok.size() - 1) + ")");
                        int index = scan.nextInt();
                        System.out.println("   Введите новое значение элемента");
                        int newNumber = scan.nextInt();
                        spisok.setElementAtPosition(index, newNumber);


                        break;
                    case 3:
                        System.out.println("   Введите номер позиции (0 - " + (spisok.size() - 1) + ")");
                        int index1 = scan.nextInt();
                        spisok.deleteElement(index1);

                        break;
                    case 4:
                        System.out.println("Размер списка = " + spisok.size());
                        break;
                    case 5:
                        System.out.println("   Введите номер позиции");
                        int index2 = scan.nextInt();

                        System.out.println("   Элемент позиции " + index2 + " - " + spisok.getElementByIndex(index2));
                        break;
                    default:
//                        System.out.println("Операция не распознана");
                        break;
                }
            } catch (InputMismatchException e) {
                System.out.println("      Введенное значение должно быть Int");
                scan.nextLine();
//            } catch (NullPointerException e) {
//                e.printStackTrace();
            }
        } while (operNumb != 6);
        System.out.println("Вы вышли! ");
    }
}
