package ru.inordic.arj.mvn.homework9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class ListMainTest {

    @Test
    void testing () throws Exception {
        ListHub listHub = new ListHub();

        Assertions.assertThrows(Exception.class, () -> listHub.deleteElement(0));
//        Assertions.assertThrows(Exception.class, () -> listHub.getElementByIndex(0));
//        Assertions.assertThrows(Exception.class, () -> listHub.setElementAtPosition(0,5));


        listHub.addLast(1);
         listHub.addLast(2);
        Assertions.assertEquals(2, listHub.size());

        listHub.addLast(10);
        Assertions.assertEquals(3, listHub.size());

        listHub.getElementByIndex(2);
        Assertions.assertEquals(10, listHub.getElementByIndex(2));

        listHub.deleteElement(1);
        Assertions.assertEquals(10, listHub.getElementByIndex(1));

        listHub.setElementAtPosition(1, 33);
        Assertions.assertEquals(33, listHub.getElementByIndex(1));
        Assertions.assertEquals(1, listHub.getElementByIndex(0));
        Assertions.assertEquals(2, listHub.getElementByIndex(2));

        Assertions.assertEquals(3, listHub.size());
    }

}